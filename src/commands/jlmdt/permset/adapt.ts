import { flags, SfdxCommand, UX } from "@salesforce/command";
import { AnyJson } from "@salesforce/ts-types";
import * as chalk from "chalk";
import * as path from "path";
import * as x2jParser from "fast-xml-parser";
import { j2xParser } from "fast-xml-parser";
import * as fs from "fs";

import {
  substringBefore,
  substringAfter,
  readFile,
  writeXMLFile,
} from "../../../utils/utilities";
import { filterMetadataTypeTag } from "../../../utils/adapt";
import { j2xOptions, x2jOptions } from "../../../config/fastXMLOptions";


const standardMissingObjects = ["AIInsightReason","AIRecordInsight","AppAnalyticsQueryRequest","AppUsageAssignment","AsyncOperationLog",
                                    "BackgroundOperation","Document","GtwyProvPaymentMethodType","PaymentGatewayLog","PrivacyConsent","PushTopic"];
const standardRecordTypeVisibilities = ["Idea.InternalIdeasIdeaRecordType"];

export default class Adapter extends SfdxCommand {
  public static examples = [
    `$ sfdx mdt:profile:adapt -p {sourcepath} -d {outputdirectory}
    Adapt a profile to be deployed to an org
  `,
  ];

  protected static flagsConfig = {
    sourcepath: flags.string({
      char: "p",
      required: true,
      description: "The path to the source metadata file",
    }),
    outputdir: flags.string({
      char: "d",
      description:
        "The output directory where to store the profile metadata file",
    }),
  };

  protected static requiresUsername = true;

  public async run(): Promise<AnyJson> {
    this.ux.startSpinner(chalk.yellowBright("Adapting Profile(s)"));
    try {
      await this.adapt(this.flags.sourcepath, this.flags.outputdir);

    } catch (e) {
      // output error
      this.ux.stopSpinner("❌");
      this.ux.error(chalk.redBright(e));
    }

    // Return an object to be displayed with --json
    return { success: true };
  }

  

  /**
   * adapt profile
   * @param sourcepath
   * @param outputdir
   */
  public async adapt(sourcepath: string, outputdir: string) {   
    if(sourcepath.endsWith('.permissionset-meta.xml')){        
      await this.cleanFile(sourcepath,outputdir);
    }else{
      //Get all file names in folder
      const files = fs.readdirSync(sourcepath, {withFileTypes: true})
                    .filter(item => !item.isDirectory())
                    .map(item => item.name);
      //Filter only profiles
      const validFiles = files.filter(name => name.endsWith('.permissionset-meta.xml'));
      validFiles.forEach(async (fileName) => {
        await this.cleanFile(sourcepath + "/" + fileName,outputdir);
      });
    }
    
  }

  public async cleanFile(sourcepath: string, outputdir: string) {
    const permSetXMLData: string = await readFile(sourcepath);
    const json2xmlParser = new j2xParser(j2xOptions);
    const permSetName: string = substringBefore(path.basename(sourcepath), ".");
    const destpath: string = outputdir
      ? `${outputdir}/${permSetName}.permissionset-meta.xml`
      : sourcepath;
    let permSetJSON;
    try{
      if (x2jParser.validate(permSetXMLData) === true) {
        // parse xml to json
        permSetJSON = x2jParser.parse(permSetXMLData, x2jOptions);
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "applicationVisibilities",
          "CustomApplication",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["application"])
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "categoryGroupVisibilities",
          "DataCategoryGroup",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["dataCategoryGroup"])
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "classAccesses",
          "ApexClass",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["apexClass"])
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "customMetadataTypeAccesses",
          "CustomObject",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["name"])
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "customPermissions",
          "CustomPermission",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["name"])
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "customSettingAccesses",
          "CustomObject",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["name"])
  
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "externalDataSourceAccesses",
          "ExternalDataSource",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["externalDataSource"])
        );
  
        // metadata api limitation
        // standard fields not listed in the CustomField metadata
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "fieldPermissions",
          "CustomField",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["field"]) ||
            !substringAfter(profileAccess["field"], ",").includes("__") // don't filter standard fields
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "flowAccesses",
          "Flow",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["flow"])
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "objectPermissions",
          "CustomObject",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["object"]) ||
            standardMissingObjects.includes(profileAccess["object"]) // do not filter standard objects not found in Metadata API
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "pageAccesses",
          "ApexPage",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["apexPage"])
        );
  
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "recordTypeVisibilities",
          "RecordType",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["recordType"]) ||
            standardRecordTypeVisibilities.includes(profileAccess["recordType"]) // do not filter standard recordTypes
        );
  
        // metadata api limitation
        // standard tabs not listed in the CustomTab metadata
        await filterMetadataTypeTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "tabVisibilities",
          "CustomTab",
          (metadataTypeList, profileAccess) =>
            metadataTypeList.includes(profileAccess["tab"]) ||
            profileAccess["tab"].startsWith("standard-") // don't filter standard tabs
        );
  
        await this.filterUserPermissionTag(
          this.org.getConnection(),
          permSetJSON.PermissionSet,
          "userPermissions"
        );
      }
  
      // convert to xml
      const formattedXml = json2xmlParser.parse(permSetJSON);
  
      // write xml file
      writeXMLFile(`${destpath}`, formattedXml);
      console.log(chalk.greenBright(`${permSetName} adapted successfully`));
    }catch(e){
      console.log(chalk.redBright(permSetName + ' failed: ' + e) + ' ❌');
      throw e;
    }
  }

  /**
   * filter user permission tags
   * @param conn
   * @param permSet
   * @param permSetAccessName
   */
  public async filterUserPermissionTag(conn, permSet, permSetAccessName) {
    const permSetAccess = permSet[permSetAccessName];
    if (permSetAccess) {
      //Get user permissions available in Org from Profile Object
      const profileDescribe = await conn.sobject("Profile").describe();
      const permSetAccessList = Array.isArray(permSetAccess)
        ? permSetAccess
        : [permSetAccess];
      const userPermissionNameList = profileDescribe.fields
        .map((userPermission) => userPermission.name)
        .filter((name) => !["Id", "Name"].includes(name))
        .map((name) => name.slice(11));
        permSet[permSetAccessName] = permSetAccessList.filter((permSetAccess) =>
        userPermissionNameList.includes(permSetAccess.name)
      );
      //console.log(`${profileAccessName} ✔️`);
    }
  }
}
